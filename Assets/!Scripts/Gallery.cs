using UnityEngine;
using UnityEngine.UI;

public class Gallery : MonoBehaviour
{
    public Image image;
    public Sprite[] sprites;
    private int count = 0;
    private void Start()
    {
        image.sprite = sprites[count];
    }
    public void Next()
    {
        count += 1;
        if (count == sprites.Length)
        {
            count = 0;
        }
        image.sprite = sprites[count];
    }
    public void Back()
    {
        count -= 1;
        if (count < 0)
        {
            count = sprites.Length -1;
        }
        image.sprite = sprites[count];
    }
    public void OpenLink(string url)
    {
        Application.OpenURL(url);
    }
}
