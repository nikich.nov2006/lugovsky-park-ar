using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlacementObject : MonoBehaviour
{
    public GameObject[] objectsToPlace;
    public Canvas canvas;
    private Text distanceText;
    private Text trackedImgText;
    private GameObject camera;
    private LineRenderer line;
    public static string trackedImgName;
    private GameObject lineTo;
    // Start is called before the first frame update
    void Start()
    {
        distanceText = GameObject.FindWithTag("DistanceText").GetComponent<Text>();
        trackedImgText = GameObject.FindWithTag("TrackedImgText").GetComponent<Text>();
        camera = GameObject.FindWithTag("MainCamera");
        line = GetComponent<LineRenderer>();
        //canvas.worldCamera = GameObject.FindWithTag("SecondCamera").GetComponent<Camera>();

        if (trackedImgName == "Case5")
        {
            objectsToPlace[0].SetActive(true);
            lineTo = objectsToPlace[0].GetComponent<RaycastObj>().linePivot;
            trackedImgText.text = "����� �������� ���������";
        }
        else if (trackedImgName == "Case4")
        {
            objectsToPlace[1].SetActive(true);
            lineTo = objectsToPlace[1].GetComponent<RaycastObj>().linePivot;
            trackedImgText.text = "���� � �������� ���������";
        }
        else if (trackedImgName == "Case3")
        {
            objectsToPlace[2].SetActive(true);
            lineTo = objectsToPlace[2].GetComponent<RaycastObj>().linePivot;
            trackedImgText.text = "������� � �������� ������� �������� �����";
        }
    }

    void Update()
    {        
        if (lineTo)
        {
            Vector3 cameraPos = new Vector3(camera.transform.position.x, camera.transform.position.y - 1, camera.transform.position.z);
            line.SetPosition(0, cameraPos);
            line.SetPosition(1, lineTo.transform.position);
            distanceText.text = Vector3.Distance(camera.transform.position, lineTo.transform.position).ToString("0.00") + " �";
        }
    }
}
