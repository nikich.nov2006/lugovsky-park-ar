using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastObj : MonoBehaviour
{
    public bool isRaycasted = false;
    private bool oldIsRaycasted = false;
    public string name;
    public GameObject linePivot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isRaycasted != oldIsRaycasted)
        {
            if (isRaycasted == true)
            {
                GetComponent<Outline>().enabled = true;
                //GetComponent<MeshRenderer>().material.color = Color.red;
            }
            else
            {
                GetComponent<Outline>().enabled = false;
                //GetComponent<MeshRenderer>().material.color = Color.blue;
            }
            oldIsRaycasted = isRaycasted;
        }
    }
}
