using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class GameController : MonoBehaviour
{
    public GameObject[] casesCanvas;
    public GameObject redirectButton;
    public Text text;
    public ARTrackedImageManager arTrackedImage;
    private string trackedImgName;
    private GameObject mainCamera;
    private GameObject currentRaycasted;
    private void Awake()
    {
        foreach( GameObject placementObj in GameObject.FindGameObjectsWithTag("PlacementObj"))
        {
            Destroy(placementObj);
        }
    }
    private void Start()
    {
        mainCamera = Camera.main.gameObject;
    }
    private void LateUpdate()
    {
        //Raycasting
        Ray ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);
        Debug.DrawRay(mainCamera.transform.position, mainCamera.transform.forward, Color.red, 100000);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && hit.transform.tag == "raycastObj")
        {
            GameObject select = hit.transform.gameObject;
            if (currentRaycasted && currentRaycasted != select)
            {
                currentRaycasted.GetComponent<RaycastObj>().isRaycasted = false;
            }
            currentRaycasted = select;
            select.GetComponent<RaycastObj>().isRaycasted = true;
        }
        else
        {
            if (currentRaycasted)
            {
                currentRaycasted.GetComponent<RaycastObj>().isRaycasted = false;
            }
            currentRaycasted = null;
        }
    }


    private void OnEnable()
    {
        arTrackedImage.trackedImagesChanged += OnTrackedImgChanged;
    }
    private void OnDisable()
    {
        arTrackedImage.trackedImagesChanged -= OnTrackedImgChanged;
    }
    void OnTrackedImgChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {
            trackedImgName = trackedImage.referenceImage.name;
            PlacementObject.trackedImgName = trackedImage.referenceImage.name;
        }
    }
    public void TrackedObjInfo()
    {
        if (trackedImgName != null)
        {
            redirectButton.SetActive(false);
            if (trackedImgName == "Case5")
            {
                casesCanvas[0].SetActive(true);
            }
            else if (trackedImgName == "Case4")
            {
                casesCanvas[1].SetActive(true);
            }
            else if (trackedImgName == "Case3")
            {
                casesCanvas[2].SetActive(true);
            }
            
        }
    }
}
