Команда "Why not" Великий Новгород
Кейс 3 https://drive.google.com/file/d/1Yyc-YWYSiKclpzAA_NhcgP1GejOHFnsd/view?usp=sharing
Кейс 4 https://drive.google.com/file/d/1bDNxRi17eMC-j0tsaP-iTTg-edmSnS7S/view?usp=sharing
Кейс 5 https://drive.google.com/file/d/1WTRk5Onoj6iUnm7TQFoEStQslNAzXYGe/view?usp=sharing
Видео презентация https://drive.google.com/file/d/1OFBn5IGLev660tl6eZ_Len3B0yz-gV_a/view?usp=sharing
 
Решением всех трех кейсов является одно приложение https://drive.google.com/file/d/11_XoHYkmx3WyaH9w8fA5M9gYpn4DlY0z/view?usp=sharing , для просмотра AR контента необходимо установить приложение и отсканировать в нём метки прикрепленные к кейсам. Приложение разработано на движке Unity, при помощи библиотек ARCore
 
История использования приложения: в Луговом парке расставлены горизонтальные стенды - метки. При сканировании метки в приложении пользователю открывается AR 3д модель объекта. 
 
Технические требования к смартфону: наличие Android 7+ и гироскопа, для отслеживания устройства в пространстве
Технические требования к меткам: нахождение метки при сканировании на высоте примерно 1м от уровня пола.
 
От расположения метки в самом Луговом парке напрямую зависит правильное позиционирование 3д моделей. На данном этапе, не имея возможности проверить решение непосредственно в парке, используются примерные расстояния между объектами и метками, измеренные по Яндекс картам. Расстояние от пользователя до Розового павильона можно увидеть вверху экрана приложения.
 
Unity проект https://gitlab.com/nikich.nov2006/lugovsky-park-ar
Общая папка проекта https://drive.google.com/drive/folders/1D2nn1hotMhhV-3cJFzizBIcx_O0cGILS?usp=sharing

